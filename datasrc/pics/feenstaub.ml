(*
   Copyright 2006 by Mark Weyer
   Maintenance modifications 2011 by the cuyo developers

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)

open Farbe
open Graphik

let schritte = 10
let kante = 64
let radmax = 48
let radstreu = 8.0
let minfarbe = 0.5
let farbexp = 2.0
let kornabstand = 20

let erzschritte = schritte-3
let erzrad = ((float_of_int radmax)+.radstreu)/.(float_of_int erzschritte)

let farbkanal u = minfarbe+.(1.0-.minfarbe)*.((Random.float 1.0) ** farbexp)
let grau = grau minfarbe

let staub u = Array.init kante (fun y -> Array.init kante (fun x ->
  if Random.int kornabstand = 0
  then
    let x,y = 2*x-kante+1, 2*y-kante+1  in
    let r2 = x*x+y*y  in
    if r2>radmax*radmax
    then None
    else
      let schritt = ((Random.float (2.0*.radstreu)) -. radstreu +.
        sqrt (float_of_int r2))/.erzrad  in
      Some (
        int_of_float (if schritt<0.0  then 0.0  else schritt),
	von_rgb (rgbrgb (farbkanal ()) (farbkanal ()) (farbkanal ())))
  else None))

let rumgebung staub x y r schritt =
  let teste x y =
    if x>=0 && x<kante && y>=0 && y<kante
    then (match staub.(y).(x)  with
    | Some (schritt',farbe)  when schritt'=schritt -> [1.0,farbe]
    | _ -> [])
    else []  in
  (teste (x-r) y) @ (teste (x+r) y) @ (teste x (y-r)) @ (teste x (y+r))

let umgebung staub x y schritt =
  let farben =
    (rumgebung staub x y 1 schritt) @ (rumgebung staub x y 2 schritt)   in
  if farben=[]
  then None
  else Some (misch farben)

let machstaub staub =
  kante,
  schritte*kante,
  Array.init (schritte*kante) (fun y ->
    let schritt,y = y/kante, y mod kante  in
    Array.init kante (fun x ->
      match staub.(y).(x) with
      |	Some (schritt',farbe)  when schritt>=schritt' && schritt<=schritt'+3
	-> if schritt<=schritt'+1  then weiss  else farbe
      | _ -> (match rumgebung staub x y 1 (schritt-1)  with
        | [] -> (match umgebung staub x y (schritt-2) with
          | None -> (match umgebung staub x y (schritt-3) with
            | None -> durchsichtig
            | Some farbe -> grau)
          | Some farbe -> farbe)
        | farben -> misch farben)))

;;

let synopsis = "feenstaub options"  in
let outname = Easyarg.register_string_with_default "-o" "Output file name"
  "feenstaub.xpm"  in

ignore (Easyarg.parse synopsis);

Random.init 3456;

let bild = machstaub (staub ())  in
  gib_xpm_aus (rgb_grau 0.0) !outname bild;

